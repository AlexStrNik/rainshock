use crc32fast::Hasher;
use hidapi::HidApi;
use palette::{encoding::Srgb, Hsv, IntoColor};
use std::{thread, time::Duration};

const DUALSHOCK4_PRODUCT_ID: u16 = 0x05C4;
const REFRESH_RATE: f64 = 60.0;
const SPEED: f64 = 1.1;

fn construct_report(red: u8, green: u8, blue: u8) -> Vec<u8> {
    let mut hasher = Hasher::new();

    // https://www.psdevwiki.com/ps4/DualShock_4#Specifications
    // https://www.psdevwiki.com/ps4/DS4-BT#HID_features_reports
    let mut report = [
        0xa2, 0x11, 0x80, 0x00, 0b11110010, 0x00, 0x00, 0x00, 0x00, red, green, blue, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00,
    ]
    .to_vec();

    hasher.update(&report);
    let crc_32 = hasher.finalize().to_le_bytes();

    report.extend_from_slice(&crc_32);
    report.remove(0);

    report
}

fn main() {
    let api = HidApi::new().unwrap();
    let dualshock = api
        .device_list()
        .find(|device| device.product_id() == DUALSHOCK4_PRODUCT_ID)
        .expect("No DualShock4 found");

    let dualshock = dualshock.open_device(&api).expect("Failed to open device");

    let mut degrees = 0.0;

    let delay = Duration::from_secs_f64(1.0 / REFRESH_RATE);

    loop {
        let color = Hsv::new(degrees, 1.0, 1.0).into_rgb::<Srgb>();
        let color = color.into_format::<u8>();

        let report = construct_report(color.red, color.green, color.blue);

        dualshock.write(&report).expect("Failed to send report");
        degrees = (degrees + SPEED) % 360.0;
        thread::sleep(delay);
    }
}
